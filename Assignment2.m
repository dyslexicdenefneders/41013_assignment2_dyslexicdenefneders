% Clear all Figures and the Console
clc
% clf

% Set the default viewing angle of the Figure
view(0, 35.7354)

%Checks serial port is free, leave at start of code
if ~isempty(instrfind)
    fclose(instrfind);
    delete(instrfind);
end
%Adds Arduino
setGlobalStop(0);
a = arduino('/dev/cu.usbserial-AI032DPG','ProMini328_3V','Libraries', 'JRodrigoTech/HCSR04'); %%Comment out to run without Arduino, and comment out the ArduinoCheck in the k loop of movements

%% ***************************** CODE FOR MAIN FUNCTION ********************************
%Load the Blender .ply files for the Environment into the Figure
%Enclosure
[f,v,data] = plyread('Enclosure Single.ply','tri');
vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;
hold on;
xOffset = -0.52;
yOffset = 0.1;
enclosure = trisurf(f,v(:,1)+ xOffset,v(:,2) + yOffset, v(:,3),'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
set(enclosure, 'edgecolor','none')

%Letter
[f,v,data] = plyread('Envelope.ply','tri');
hold on;
xOffset = -0.15;
yOffset = -0.15;
zOffset = 0.04;
envelope = trisurf(f,v(:,1)+ xOffset,v(:,2) + yOffset, v(:,3) + zOffset);
set(envelope, 'edgecolor','none')

%Stamps
stamps = 4;
for i = 1:stamps
    [f,v,data] = plyread('Stamp.ply','tri');
    hold on;
    xOffset = -0.08 + (0.03*i - 0.03);
    yOffset = -0.3;
    zOffset = 0.03;
    stamp = trisurf(f,v(:,1)+ xOffset,v(:,2) + yOffset, v(:,3) + zOffset);
    set(stamp, 'edgecolor','none')
end

%Invisible Collision barriers
centerpnt = [-0.01,-0.2,0.3];
side = 0.6;
plotOptions.plotFaces = false;
centerpnt-side/2;
centerpnt+side/2;
[vertex,faces,faceNormals] = RectangularPrism(centerpnt-side/2, centerpnt+side/2,plotOptions);

%Conveyor Collision Faces
[vertex1,faces1,faceNormals1] = RectangularPrism([-0.51,-0.3,0], [0.5,-0.12,0.04],plotOptions);

camlight;
axis equal;
view(3);
hold on;

% **************************** END CODE FOR MAIN FUNCTION ******************************

%% Build the DoBot
% Define the 5 Links of the Dobot, including limitations
L1 = Link('d',0.1,'a',0,'alpha',pi/2,'offset',-pi/2,'qlim',deg2rad([-135 135]));
L2 = Link('d',0,'a',0.135,'alpha',pi,'qlim',deg2rad([5 80]));
L3 = Link('d',0,'a',0.160,'alpha',0,'qlim',deg2rad([15 170]));
L4 = Link('d',0,'a',0.05,'alpha',3*pi/2,'qlim',deg2rad([-180 180]));
L5 = Link('d',0.05,'a',0,'alpha',0,'qlim',deg2rad([-85 85]));

% Generate a 3 Link and a 5 Link Robot
robotWithout = SerialLink([L1 L2 L3],'name','3Link');
robotWith = SerialLink([L1 L2 L3 L4 L5],'name','5Link');

%% Setup Workspace and Parameters

workspace = [-0.4 0.4 -0.4 0.4 -0.1 0.6];
scale = 0.5;

% Mask R P Y
% This is so ikine solutions will achieve the position regardless of orientation
m = [1 1 1 0 0 0];

%BASES OF ROBOTS MUST BE THE SAME
robotWithout.base = transl(0.0,0.0,0.0);
robotWith.base = transl(0.0,0.0,0.0);

%Appropriate Guess Pose for each Robot Model
qGuess3Link = deg2rad([0,65,127]);
qGuess5Link = deg2rad([0,65,127,0,0]);

robotWith.plot(qGuess5Link,'workspace',workspace,'scale',scale)
hold on

%% Begin Plotting

% Iterate through the Links and Load the appropriate .ply file onto the model
for linkIndex = 0:robotWith.n
    [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['Dobot',num2str(linkIndex),'.ply'],'tri');
    robotWith.faces{linkIndex+1} = faceData;
    robotWith.points{linkIndex+1} = vertexData;
end

% Define the "home point" and determine the pose needed to achieve this position
% NOTE: This position is only roughly reached as q4 and q5 are constrained after ikine is solved
trHome = transl(0,-0.2,0.2);
qHome3L = robotWithout.ikine(trHome, qGuess3Link, m);
qHome5L = [qHome3L(1,1), qHome3L(1,2), qHome3L(1,3), (qHome3L(1,2)) - (qHome3L(1,3)), 0];

% Display the robot with the model overlay using plot3d
robotWith.plot3d(qHome5L,'noarrow','workspace',workspace,'scale',0.1);
camlight
view(15, 35.7354)
robotWith.delay = 0;


% Define the "stamping point" where stamps are applied to the envelopes
trStampOn = transl(-0.13,-0.24,0.045);
plot3(-0.13,-0.24,0.045, 'marker','*')
% Determine the Offset Position of Link 3 (For trStampOn)
% Since the robot base = 0,0,0 the change in x,y,z must be the x,y,z values of trStampOn
x1Change = trStampOn(1,4);
y1Change = trStampOn(2,4);
z1Change = trStampOn(3,4);
% Calculate the straight line length from robot base to goal position in x and y
hypot = sqrt(x1Change^2 + y1Change^2);
% Knowing the offset is in L shape 0.5 x 0.5 calculate new hypotenuse
hypotNew = hypot - 0.05;
% Calculate the angle that the goal is from robot base in x and y
theta1 = atan(abs(x1Change)/abs(y1Change));
% Determine the x and y offset from robot base, of the 3rd Link Goal position
x1 = hypotNew*sin(theta1);
y1 = hypotNew*cos(theta1);
% Since all distances are positive, check if the translation needs to be in the negative or positive x direction
if trStampOn(1,4) < 0
    trStampOnOffset = transl(-x1,-y1,(0.05+z1Change));
else
    trStampOnOffset = transl(x1,-y1,(0.05+z1Change));
end

% Use ikine, with an appropriate guess pose and the mask, to solve the 3rd link reaching the offset position
q3LStampOffset = robotWithout.ikine(trStampOnOffset, qGuess3Link, m);

% Apply the values from q3LStampOffset to the first 3 joints of the 5 Link q value, and constrain q4 = q2-q3, q5 = 0
% The maths above has accounted for the change that this constraint applies, and results in the end effector reaching the original goal
q5LStamp = [q3LStampOffset(1,1), q3LStampOffset(1,2), q3LStampOffset(1,3), (q3LStampOffset(1,2)) - (q3LStampOffset(1,3)), 0]

% Apply this same maths for each of the stamps, to determine all q values for the 5 Link to reach all Stamps
for r = 1:stamps
    trStamp = transl(-0.1+(r*0.03),-0.31,0.04);
    %Determine Offset Position for Link 3
    xChange(1,r) = trStamp(1,4);
    yChange(1,r) = trStamp(2,4);
    zChange(1,r) = trStamp(3,4);
    hypot(1,r) = sqrt(xChange(1,r)^2 + yChange(1,r)^2);
    hypotNew(1,r) = hypot(1,r) - 0.05;
    theta(1,r) = atan(abs(xChange(1,r)/abs(yChange(1,r))));
    x(1,r) = hypotNew(1,r)*sin(theta(1,r));
    y(1,r) = hypotNew(1,r)*cos(theta(1,r));
    if xChange(1,r) < 0
        trStampOffset = transl((-1*x(1,r)),(-1*y(1,r)),(0.05+zChange(1,r)));
    else
        trStampOffset = transl((x(1,r)),(-1*y(1,r)),(0.05+zChange(1,r)));
    end
    
    %Solve inverse kinematics for 3 Link Robot to reach the offsets
    qThreeL(r,:) = robotWithout.ikine(trStampOffset, qGuess3Link, m)
    temp = qThreeL(r,:);
    %Apply these q values to the first 3 Links of the 5Link
    qFiveL(r,:) = [temp(1,1), temp(1,2), temp(1,3), (temp(1,2)) - (temp(1,3)), 0];
    
end

% The number of steps that jtraj will use for the qMatrix
steps = 35;
movements = 9;

% This loop will plot the Robots Movement using animate, whilst checking for collisions 
% and also checking for interrupts generated by the sensors which are interfaced with the Arduino
for k = 1:movements
    
    if k == 1 && getGlobalStop == 0
        qMatrix = jtraj(qHome5L,qFiveL(1,:),steps);
        result = true(steps,1);
        for i = 1: steps
            % Check for collision between Enclosure and Conveyor
            result(i) = isCollision(robotWith,qMatrix(i,:),faces,vertex,faceNormals);
            result(i) = isCollision(robotWith,qMatrix(i,:),faces1,vertex1,faceNormals1);
            % For speed sake, every fifth step, check for interupts from the sensors
            if (mod(i,5) == 0) && (getGlobalStop == 0)
                ArduinoCheck(a);
            end
            % If an interrupt is activated, do not allow the robot to continue advancing
            if getGlobalStop == 1
                disp('STOP!!!')
                if i == steps - 1
                    qMatrix(i,:) = qMatrix(i,:);
                else
                    qMatrix(i+1,:) = qMatrix(i,:);
                end
            % Continue plotting the robot
            else if getGlobalStop == 0
                    robotWith.animate(qMatrix(i,:))
                    drawnow()
                end
            end
        end
    end
    if k == 2 && getGlobalStop == 0
        qMatrix = jtraj(qFiveL(1,:),q5LStamp,steps);
        result = true(steps,1);
        for i = 1: steps
            result(i) = isCollision(robotWith,qMatrix(i,:),faces,vertex,faceNormals);
            result(i) = isCollision(robotWith,qMatrix(i,:),faces1,vertex1,faceNormals1);
            if (mod(i,5) == 0) && (getGlobalStop == 0)
                ArduinoCheck(a);
            end
            if getGlobalStop == 1
                disp('STOP!!!')
                if i == steps - 1
                    qMatrix(i,:) = qMatrix(i,:);
                else
                    qMatrix(i+1,:) = qMatrix(i,:);
                end
            else if getGlobalStop == 0
                    robotWith.animate(qMatrix(i,:))
                    drawnow()
                end
            end
        end
    end
    
    if k == 3 && getGlobalStop == 0
        qMatrix = jtraj(q5LStamp,qFiveL(2,:),steps);
        result = true(steps,1);
        for i = 1: steps
            result(i) = isCollision(robotWith,qMatrix(i,:),faces,vertex,faceNormals);
            result(i) = isCollision(robotWith,qMatrix(i,:),faces1,vertex1,faceNormals1);
            if (mod(i,5) == 0) && (getGlobalStop == 0)
                ArduinoCheck(a);
            end
            if getGlobalStop == 1
                disp('STOP!!!')
                if i == steps - 1
                    qMatrix(i,:) = qMatrix(i,:);
                else
                    qMatrix(i+1,:) = qMatrix(i,:);
                end
            else if getGlobalStop == 0
                    robotWith.animate(qMatrix(i,:))
                    drawnow()
                end
            end
        end
    end
    
    if k == 4 && getGlobalStop == 0
        qMatrix = jtraj(qFiveL(2,:),q5LStamp,steps);
        result = true(steps,1);
        for i = 1: steps
            result(i) = isCollision(robotWith,qMatrix(i,:),faces,vertex,faceNormals);
            result(i) = isCollision(robotWith,qMatrix(i,:),faces1,vertex1,faceNormals1);
            if (mod(i,5) == 0) && (getGlobalStop == 0)
                ArduinoCheck(a);
            end
            if getGlobalStop == 1
                disp('STOP!!!')
                if i == steps - 1
                    qMatrix(i,:) = qMatrix(i,:);
                else
                    qMatrix(i+1,:) = qMatrix(i,:);
                end
            else if getGlobalStop == 0
                    robotWith.animate(qMatrix(i,:))
                    drawnow()
                end
            end
        end
    end
    
    if k == 5 && getGlobalStop == 0
        qMatrix = jtraj(q5LStamp,qFiveL(3,:),steps);
        result = true(steps,1);
        for i = 1: steps
            result(i) = isCollision(robotWith,qMatrix(i,:),faces,vertex,faceNormals);
            result(i) = isCollision(robotWith,qMatrix(i,:),faces1,vertex1,faceNormals1);
            if (mod(i,5) == 0) && (getGlobalStop == 0)
                ArduinoCheck(a);
            end
            if getGlobalStop == 1
                disp('STOP!!!')
                if i == steps - 1
                    qMatrix(i,:) = qMatrix(i,:);
                else
                    qMatrix(i+1,:) = qMatrix(i,:);
                end
            else if getGlobalStop == 0
                    robotWith.animate(qMatrix(i,:))
                    drawnow()
                end
            end
        end
    end
    
    if k == 6 && getGlobalStop == 0
        qMatrix = jtraj(qFiveL(3,:),q5LStamp,steps);
        result = true(steps,1);
        for i = 1: steps
            result(i) = isCollision(robotWith,qMatrix(i,:),faces,vertex,faceNormals);
            result(i) = isCollision(robotWith,qMatrix(i,:),faces1,vertex1,faceNormals1);
            if (mod(i,5) == 0) && (getGlobalStop == 0)
                ArduinoCheck(a);
            end
            if getGlobalStop == 1
                disp('STOP!!!')
                if i == steps - 1
                    qMatrix(i,:) = qMatrix(i,:);
                else
                    qMatrix(i+1,:) = qMatrix(i,:);
                end
            else if getGlobalStop == 0
                    robotWith.animate(qMatrix(i,:))
                    drawnow()
                end
            end
        end
    end
    
    if k == 7 && getGlobalStop == 0
        qMatrix = jtraj(q5LStamp,qFiveL(4,:),steps);
        result = true(steps,1);
        for i = 1: steps
            result(i) = isCollision(robotWith,qMatrix(i,:),faces,vertex,faceNormals);
            result(i) = isCollision(robotWith,qMatrix(i,:),faces1,vertex1,faceNormals1);
            if (mod(i,5) == 0) && (getGlobalStop == 0)
                ArduinoCheck(a);
            end
            if getGlobalStop == 1
                disp('STOP!!!')
                if i == steps - 1
                    qMatrix(i,:) = qMatrix(i,:);
                else
                    qMatrix(i+1,:) = qMatrix(i,:);
                end
            else if getGlobalStop == 0
                    robotWith.animate(qMatrix(i,:))
                    drawnow()
                end
            end
        end
    end
    
    if k == 8 && getGlobalStop == 0
        qMatrix = jtraj(qFiveL(4,:),q5LStamp,steps);
        result = true(steps,1);
        for i = 1: steps
            result(i) = isCollision(robotWith,qMatrix(i,:),faces,vertex,faceNormals);
            result(i) = isCollision(robotWith,qMatrix(i,:),faces1,vertex1,faceNormals1);
            if (mod(i,5) == 0) && (getGlobalStop == 0)
                ArduinoCheck(a);
            end
            if getGlobalStop == 1
                disp('STOP!!!')
                if i == steps - 1
                    qMatrix(i,:) = qMatrix(i,:);
                else
                    qMatrix(i+1,:) = qMatrix(i,:);
                end
            else if getGlobalStop == 0
                    robotWith.animate(qMatrix(i,:))
                    drawnow()
                end
            end
        end
    end
    
    if k == 9 && getGlobalStop == 0
        qMatrix = jtraj(q5LStamp,qHome5L,steps);
        result = true(steps,1);
        for i = 1: steps
            result(i) = isCollision(robotWith,qMatrix(i,:),faces,vertex,faceNormals);
            result(i) = isCollision(robotWith,qMatrix(i,:),faces1,vertex1,faceNormals1);
            if (mod(i,5) == 0) && (getGlobalStop == 0)
                ArduinoCheck(a);
            end
            if getGlobalStop == 1
                disp('STOP!!!')
                if i == steps - 1
                    qMatrix(i,:) = qMatrix(i,:);
                else
                    qMatrix(i+1,:) = qMatrix(i,:);
                end
            else if getGlobalStop == 0
                    robotWith.animate(qMatrix(i,:))
                    drawnow()
                end
            end
        end
    end
    
end

clear a
clear t
teachUpdated(robotWith);