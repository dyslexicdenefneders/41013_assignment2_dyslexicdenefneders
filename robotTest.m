clf;
clc;
close all;
clear all;
view(131.4, 24.7354)
%% SETTING UP THE SWAYER ROBOTS
%%Define DH parameters for the links
L1 = Link('d',0.1,'a',0,'alpha',pi/2,'qlim',deg2rad([-135 135]))
L2 = Link('d',0,'a',0.135,'alpha',pi,'qlim',deg2rad([-5 85]))
L3 = Link('d',0,'a',0.160,'alpha',0,'qlim',deg2rad([-10 95]))
L4 = Link('d',0,'a',0,'alpha',3*pi/2,'qlim',deg2rad([-180 180]))
L5 = Link('d',0.05,'a',0.05,'alpha',0,'qlim',deg2rad([-0.001 0.001]))
robot = SerialLink([L1,L2,L3,L4,L5],'name','robot'); %%Generate the model for sawyer 2
zeroPos = zeros(1,5); %%Starting point
extensionPos = [0 deg2rad(-90) 0 0 0];
workspace = [-0.4 0.4 -0.4 0.4 -0.1 0.6];  
scale = 0.5;


robot.base = transl(0,0,0);
robot.plot(zeroPos,'workspace',workspace,'scale',scale);
robot.teachUpdated();

