a = arduino('/dev/cu.usbserial-AI032DPG','ProMini328_3V','Libraries', 'JRodrigoTech/HCSR04');
sensor1 = addon(a, 'JRodrigoTech/HCSR04', 'D2', 'D3');
t1 = readTravelTime(sensor1);
sprintf('Current distance of sensor 1 is %.4f meters\n', 340*t1/2)
dist1 = 340*t1/2;
clear sensor1
sensor2 = addon(a, 'JRodrigoTech/HCSR04', 'D4', 'D5');
t2 = readTravelTime(sensor2);
sprintf('Current distance of sensor 2 is %.4f meters\n', 340*t2/2)
dist2 = 340*t2/2;
clear sensor2
sensor3 = addon(a, 'JRodrigoTech/HCSR04', 'D6', 'D7');
t3 = readTravelTime(sensor3);
sprintf('Current distance of sensor 3 is %.4f meters\n', 340*t3/2)
dist3 = 340*t3/2;
clear sensor3
if (dist1<0.51)||(dist2<0.51)||(dist3<0.51)
    disp('STOP!!!')
end

%Push Button testing
% configurePin(a, 'A0', 'AnalogInput');
% push_button = readVoltage(a,'A0');
% while(1)
% push_button = readVoltage(a,'A0')
% if push_button == 5
%     disp('PUSHED')
%     for i = 1:20
%         writeDigitalPin(a,'D10',1);
%         writeDigitalPin(a,'D11',1);
%         writeDigitalPin(a,'D12',1);
%         writeDigitalPin(a,'D13',1);
%         pause(0.05);
%         writeDigitalPin(a,'D10',0);
%         writeDigitalPin(a,'D11',0);
%         writeDigitalPin(a,'D12',0);
%         writeDigitalPin(a,'D13',0);
%         pause(0.05);
%     end
%     ImperialMarch(a);
%     stop(t);
% else
%     disp('RELEASED')
%     writeDigitalPin(a,'D10',0);
%     writeDigitalPin(a,'D11',0);
%     writeDigitalPin(a,'D12',0);
%     writeDigitalPin(a,'D13',0);
% end
% end



%% LED Testing
%  for i = 1:20
%         writeDigitalPin(a,'D10',1);
%         writeDigitalPin(a,'D11',1);
%         writeDigitalPin(a,'D12',1);
%         writeDigitalPin(a,'D13',1);
%         pause(0.05);
%         writeDigitalPin(a,'D10',0);
%         writeDigitalPin(a,'D11',0);
%         writeDigitalPin(a,'D12',0);
%         writeDigitalPin(a,'D13',0);
%         pause(0.05);
%     end
%
% ImperialMarch(a);
clear a
