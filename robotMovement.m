function robotMovement(robot,X1_,X2_,stop)
%Robot is the robot model
%X1 is the transform of the current position
%X2 is the transform of the desired position
%stop is a global varible that is set to 1 if the robot needs to stop
x1 = X1_';
x2 = X2_';
deltaT = 0.05;
steps = 20;
x = nan(3,steps);
s = lspb(0,1,steps);
for i = 1:steps
    x(:,i) = x1*(1-s(i))+s(i)*x2;
end
qMatrix = nan(steps,3);
qMatrix(1,:) = p2.ikine(T1, [0,0],M);

for i =1:steps-1
    xdot = (1/deltaT)*(x(:,i+1)-x(:,i));
    J = robot.jacob0(qMatrix(i,:));
    J = J(1:2,:);
    Plyread_Collision(robot);
    if stop == 1 %% GLOBAL VARIABLE IF COLLISION DETECTED, SENSORS TRIGGERED OR PUSH BUTTON PUSHED
        qdot = 0; %Will stop the robot
    else
        qdot = inv(J)*xdot;    
    end
    qMatrix(i+1,:) = qMatrix(i,:) + deltaT*qdot';
end
    robot.plot(qMatrix,'trail','r-');
end
