%Checks serial port is free, leave at start of code
if ~isempty(instrfind)
     fclose(instrfind);
      delete(instrfind);
end
%Adds Arduino 
a = arduino('/dev/cu.usbserial-AI032DPG','ProMini328_3V','Libraries', 'JRodrigoTech/HCSR04');
% sensor1 = addon(a, 'JRodrigoTech/HCSR04', 'D2', 'D3');
% sensor2 = addon(a, 'JRodrigoTech/HCSR04', 'D4', 'D5');
% sensor3 = addon(a, 'JRodrigoTech/HCSR04', 'D6', 'D7');
% sensorArray = [sensor1,sensor2,sensor3];
% setSensorArray(sensorArray);
%Defines timer, runs the callback function at the frequency dicated by the
%period
t = timer('executionMode','fixedRate','TimerFcn',{@ArduinoCallback,a},'Period',1);

%Starts the timer
start(t)

%Clears the arduino and timer
% clear sensor1
% clear sensor2
% clear sensor3
clear a
clear t
