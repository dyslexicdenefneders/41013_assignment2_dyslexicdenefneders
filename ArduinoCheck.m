function ArduinoCheck(a)
range = 0.51;   %Defines the range of the sensors
configurePin(a, 'A0', 'AnalogInput');   %Defines the pin for the push button
push_button = readVoltage(a,'A0');  %Determines if the push button has been pressed

%% Push Button
if push_button == 5 %If the push button has been pressed
    disp('PUSHED')
    for i = 1:20    %Flash the LEDs on and off 20 times
        writeDigitalPin(a,'D10',1);
        writeDigitalPin(a,'D11',1);
        writeDigitalPin(a,'D12',1);
        writeDigitalPin(a,'D13',1);
        pause(0.05);
        writeDigitalPin(a,'D10',0);
        writeDigitalPin(a,'D11',0);
        writeDigitalPin(a,'D12',0);
        writeDigitalPin(a,'D13',0);
        pause(0.05);
    end
    ImperialMarch(a);   %Lord Vadar approaches (Alarm sounds)
    %Stops the timer
    setGlobalStop(1);
    return; %Exits the callback
else
    disp('RELEASED')
    writeDigitalPin(a,'D10',0);
    writeDigitalPin(a,'D11',0);
    writeDigitalPin(a,'D12',0);
    writeDigitalPin(a,'D13',0);
end

%% Sensors - gets the distance read by the sensor calculated by the travel time, then clears the sensor
%Sensor 1
sensor1 = addon(a, 'JRodrigoTech/HCSR04', 'D2', 'D3');
t1 = readTravelTime(sensor1);
sprintf('Current distance of sensor 1 is %.4f meters\n', 340*t1/2)
dist1 = 340*t1/2;
clear sensor1
%Sensor 2
sensor2 = addon(a, 'JRodrigoTech/HCSR04', 'D4', 'D5');
t2 = readTravelTime(sensor2);
sprintf('Current distance of sensor 2 is %.4f meters\n', 340*t2/2)
dist2 = 340*t2/2;
clear sensor2
%Sensor 3
sensor3 = addon(a, 'JRodrigoTech/HCSR04', 'D6', 'D7');
t3 = readTravelTime(sensor3);
sprintf('Current distance of sensor 3 is %.4f meters\n', 340*t3/2)
dist3 = 340*t3/2;
clear sensor3
%Checks if something has broken the light field (there has been a reading
%less than the range
if dist1<range||dist2<range||dist3<range
    for i = 1:20    %Flash LEDs for 20 times
        writeDigitalPin(a,'D10',1);
        writeDigitalPin(a,'D11',1);
        writeDigitalPin(a,'D12',1);
        writeDigitalPin(a,'D13',1);
        pause(0.05);
        writeDigitalPin(a,'D10',0);
        writeDigitalPin(a,'D11',0);
        writeDigitalPin(a,'D12',0);
        writeDigitalPin(a,'D13',0);
        pause(0.05);
    end
    ImperialMarch(a);
    setGlobalStop(1);
    return;
end

end
