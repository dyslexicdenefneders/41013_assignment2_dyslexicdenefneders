% Create an ardunio object
a = arduino('/dev/cu.usbserial-AI032DPG','ProMini328_3V','Libraries', 'JRodrigoTech/HCSR04');
brightness_step = (5-0)/20;
range = 1.5;
readingCounter = 0;
tempDist = 0;
configurePin(a, 'A0', 'AnalogInput')
push_button = readVoltage(a,'A0')
%%Push button test, if pushed flash LEDs and play the march
% while readingCounter < 3
%     push_button = readVoltage(a,'A0')
%     if push_button == 0
%         disp('PUSHED')
%         for i = 1:10
%             writeDigitalPin(a,'D11',1);
%             pause(0.1);
%             writeDigitalPin(a,'D11',0);
%             pause(0.1);
%         end
%         ImperialMarch(a);
%         readingCounter = readingCounter + 1;
%     else
%         disp('RELEASED')
%         writeDigitalPin(a,'D11',0);
%     end
% end


% %% main loop
sensor = addon(a,'JRodrigoTech/HCSR04','D12','D13'); %Trigger then echo pin
while(readingCounter < 3)
    push_button = readVoltage(a,'A0')
    if push_button == 0
        disp('PUSHED')
        for i = 1:10
            writeDigitalPin(a,'D11',1);
            pause(0.1);
            writeDigitalPin(a,'D11',0);
            pause(0.1);
        end
        ImperialMarch(a);
        readingCounter = 10;
    else
        disp('RELEASED')
        writeDigitalPin(a,'D11',0);
    end
    dist = readDistance(sensor)
    pause(0.2);
    if dist < range
        readingCounter + 1;
        tempDist = tempDist + dist;
    else
        readingCounter = 0;
    end
    if readingCounter == 3
        finalDist = tempDist/readingCounter;
        for i = 1:10
            writeDigitalPin(a,'D11',1);
            pause(0.1);
            writeDigitalPin(a,'D11',0);
            pause(0.1);
        end
        ImperialMarch(a);
    end
end

% % End communication
clear a






%USEFUL http://au.mathworks.com/help/supportpkg/arduinoio/examples/getting-started-with-matlab-support-package-for-arduino-hardware.html