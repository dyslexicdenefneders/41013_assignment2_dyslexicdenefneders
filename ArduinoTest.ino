
#include <Servo.h> 

long duration, cm=0;
const int pingPin = 8;
int st=0;

void setup() 
{ 
  Serial.begin(9600);
 } 
 
 
void loop() 
{ 
  data();  
  Serial.print(pos); Serial.print( " "); Serial.println(cm);
  delay(30);
}


void data()
{
  pinMode(pingPin, OUTPUT);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(pingPin, LOW);
  pinMode(pingPin, INPUT);
  duration = pulseIn(pingPin, HIGH);
  // convert the time into a distance
  cm = microsecondsToCentimeters(duration);  
  if (cm>300)
  {
    cm = 300;
  }
}

long microsecondsToCentimeters(long microseconds)
{
  return microseconds / 29 / 2;
}


