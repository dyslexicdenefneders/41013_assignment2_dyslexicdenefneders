a = arduino('/dev/cu.usbserial-AI032DPG','ProMini328_3V','Libraries', 'JRodrigoTech/HCSR04');
sensor = addon(a,'JRodrigoTech/HCSR04','D12','D13'); %Trigger then echo pin
while(1)
    
dist = readDistance(sensor);
sprintf('Current distance is %.4f meters\n', dist)
t = readTravelTime(sensor);
% sprintf('Current distance based on t is %.4f meters\n', 340*t/2)
pause(1.5);
end
clear sensor;
clear a;