%% TO DO
% Blender ply files for Alan: protrude part out of z, align to x. Rotation
% around y. 
% Explore plotting a plane, use to represent walls of enclosure, instead of
% directly using parts from plyread, since the faces/vertices etc will be
% too complicated - use placeholder planes instead, that can handle
% collision detection representative to the walls. 3 Planes - back wall,
% side walls.

function [  ] = plyread_test( )

clf;
clc;
close all;
clear all;
set(0,'DefaultFigureWindowStyle','docked')

% ***************************** CODE FOR MAIN FUNCTION ********************************
%Enclosure
[f,v,data] = plyread('Enclosure Single.ply','tri');
vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;
hold on;
xOffset = -0.52;
yOffset = 0.1;
enclosure = trisurf(f,v(:,1)+ xOffset,v(:,2) + yOffset, v(:,3),'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');
set(enclosure, 'edgecolor','none')

%Letter
[f,v,data] = plyread('Envelope.ply','tri');
hold on;
xOffset = -0.15;
yOffset = -0.15;
zOffset = 0.04;
envelope = trisurf(f,v(:,1)+ xOffset,v(:,2) + yOffset, v(:,3) + zOffset);
set(envelope, 'edgecolor','none')

%Stamps
for i = 1:4
    [f,v,data] = plyread('Stamp.ply','tri');
    hold on;
    xOffset = -0.08 + (0.03*i - 0.03);
    yOffset = -0.3;
    zOffset = 0.03;
    stamp = trisurf(f,v(:,1)+ xOffset,v(:,2) + yOffset, v(:,3) + zOffset);
    set(stamp, 'edgecolor','none')
end

%Invisible Collision barriers
centerpnt = [-0.01,-0.2,0.3];
side = 0.6;
plotOptions.plotFaces = false;
centerpnt-side/2;
centerpnt+side/2;
[vertex,faces,faceNormals] = RectangularPrism(centerpnt-side/2, centerpnt+side/2,plotOptions);

%Conveyor Collision Faces
[vertex1,faces1,faceNormals1] = RectangularPrism([-0.51,-0.3,0], [0.5,-0.12,0.04],plotOptions);

camlight;
axis equal;
view(3);
hold on;

% **************************** END CODE FOR MAIN FUNCTION ******************************

% 2.1: Make a 3DOF model
L1 = Link('d',0.1,'a',0,'alpha',pi/2,'qlim',deg2rad([-135 135]));
L2 = Link('d',0,'a',0.135,'alpha',pi,'qlim',deg2rad([-5 85]));
L3 = Link('d',0,'a',0.160,'alpha',0,'qlim',deg2rad([-10 95]));
L4 = Link('d',0,'a',0,'alpha',3*pi/2,'qlim',deg2rad([-0.001 0.001]));
L5 = Link('d',0.05,'a',0.05,'alpha',0,'qlim',deg2rad([-0.001 0.001]));
robot = SerialLink([L1,L2,L3,L4,L5],'name','robot'); %%Generate the model for sawyer 2
q = [pi/2,0,0,0,0];
workspace = [-0.4 0.4 -0.4 0.4 -0.1 0.6];  
scale = 0.5;                                      
robot.base = transl(0,0,0);
robot.plot(q,'workspace',workspace,'scale',scale);

% Start and end joint values for testing
q1 = [-pi/4,0,0,0,0];
q2 = [-pi/2,pi/5,pi/2,0,0];
steps = 50;
qMatrix = jtraj(q1,q2,steps);

% Checks collision against walls, then against conveyor. Lastly animates robot
result = true(steps,1);
for i = 1: steps
    result(i) = IsCollision(robot,qMatrix(i,:),faces,vertex,faceNormals);
    result(i) = IsCollision(robot,qMatrix(i,:),faces1,vertex1,faceNormals1);
    robot.animate(qMatrix(i,:));
end
end


%% IsCollision
% Given a robot model (robot), and trajectory (i.e. joint state vector) (qMatrix)
% and triangle obstacles in the environment (faces,vertex,faceNormals)
function result = IsCollision(robot,qMatrix,faces,vertex,faceNormals)
result = false;

for qIndex = 1:size(qMatrix,1)
    q = qMatrix(qIndex,:);
    % Get the transform of every joint (i.e. start and end of every link)
    tr = zeros(4,4,robot.n+1);
    [tr,all] = robot.fkine(q);

    % Go through each link and also each triangle face
    for i = 1 : size(all,3)-1    
        for faceIndex = 1:size(faces,1)
            vertOnPlane = vertex(faces(faceIndex,1)',:);
            [intersectP,check] = LinePlaneIntersection(faceNormals(faceIndex,:),vertOnPlane,all(1:3,4,i)',all(1:3,4,i+1)'); 
            if check == 1 && IsIntersectionPointInsideTriangle(intersectP,vertex(faces(faceIndex,:)',:))
                plot3(intersectP(1),intersectP(2),intersectP(3),'g*');
                setGlobalStop(1);
%                 error('Collision detected. Exiting movement function'); % Built in collision cancelling
            end
        end    
    end
end
end

%% IsIntersectionPointInsideTriangle
% Given a point which is known to be on the same plane as the triangle
% determine if the point is 
% inside (result == 1) or 
% outside a triangle (result ==0 )
function result = IsIntersectionPointInsideTriangle(intersectP,triangleVerts)

u = triangleVerts(2,:) - triangleVerts(1,:);
v = triangleVerts(3,:) - triangleVerts(1,:);

uu = dot(u,u);
uv = dot(u,v);
vv = dot(v,v);

w = intersectP - triangleVerts(1,:);
wu = dot(w,u);
wv = dot(w,v);

D = uv * uv - uu * vv;

% Get and test parametric coords (s and t)
s = (uv * wv - vv * wu) / D;
if (s < 0.0 || s > 1.0)        % intersectP is outside Triangle
    result = 0;
    return;
end

t = (uv * wu - uu * wv) / D;
if (t < 0.0 || (s + t) > 1.0)  % intersectP is outside Triangle
    result = 0;
    return;
end

result = 1;                      % intersectP is in Triangle
end