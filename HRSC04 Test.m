clclc;
clear all;
%define serial port
s1 = serial('COM12');             
s1.BaudRate=9600;                               
fopen(s1);
%Draw Sonar Data
while(1)
data = fscanf(s1);
[th, r] = strtok(data);
th = real(str2num(th));
r = str2num(r);
[x0, y0] = pol2cart(th*0.0174532925, 100);
[x, y] = pol2cart(th*0.0174532925, r);
set(h(th),'XData',[x0,x]);
set(h(th),'YData',[y0,y]);
end
fclose(s1);


