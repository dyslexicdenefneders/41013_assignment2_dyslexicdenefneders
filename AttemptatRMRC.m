clf;
clc;
clear all;
close all;

L1 = Link('d',0.1,'a',0,'alpha',pi/2,'offset',-pi/2,'qlim',deg2rad([-135 135]))
L2 = Link('d',0,'a',0.135,'alpha',pi,'qlim',deg2rad([5 80]))
L3 = Link('d',0,'a',0.160,'alpha',0,'qlim',deg2rad([15 170]))
L4 = Link('d',0,'a',0.05,'alpha',3*pi/2,'qlim',deg2rad([-180 180]))
L5 = Link('d',0.05,'a',0,'alpha',0,'qlim',deg2rad([-85 85]))

robotWith = SerialLink([L1 L2 L3 L4 L5],'name','5Link')   
q0 = zeros(1,5);
T1 = [eye(3) [1.5 1 0]'; zeros(1,3) 1]; 
% T2 = [transl(1.5,-1,0);
M = [1 1 zeros(1,4)];
q1 = robotWith.ikcon(T1,q0);
% q2 = p2.ikine(T2,[0 0],M);
% qMatrix = jtraj(q1,q2,20);
% p2.plot(qMatrix,'trail','r-');

x1 = [1.5 1 0]';
x2 = [1.5 -1 0]';
deltaT = 0.05;
steps = 20;
x = nan(3,steps);
s = lspb(0,1,steps);
for i = 1:steps
    x(:,i) = x1*(1-s(i))+s(i)*x2;
end
qMatrix = nan(steps,5);
qMatrix(1,:) = q1;

for i =1:steps-1
   xdot = (1/deltaT)*(x(:,i+1)-x(:,i));
   J = robotWith.jacob0(qMatrix(i,:));
   J = J(1:3,:);
   if i>10
       qdot = 0;
   else
   qdot = inv(J)*xdot;                                                     % Solve velocitities via RMRC
   end
   qMatrix(i+1,:) = qMatrix(i,:) + deltaT*qdot';
end

    robotWith.plot(qMatrix,'trail','r-');